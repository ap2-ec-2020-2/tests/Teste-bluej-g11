import java.util.ArrayList;

public class Cadastro
{
    String nome, sobrenome, nomeCompleto;
    String cpf;
    int idade;
    int dataEntrada; 
    String nomeResponsavel, contatoResponsavel;
    ArrayList<String> medicamentos = new ArrayList<String> (); //lista dos medicamentos que o morador toma. Podem ser recebidos através da ficha médica do paciente, na classe Paciente

    String setNomeCompleto() {
        String nomeCompleto = this.nome + " " + this.sobrenome;
        return nomeCompleto;
    }
    void setNomeCompleto(String nomeCompleto) {
        this.nomeCompleto = nomeCompleto;
    }
    String getcpf() {
        return cpf;
    }
    int getIdade() {
        return idade;
    }
    void setIdade(int idade) {
        this.idade = idade;
    }
    String getNomeResponsavel() {
        return nomeResponsavel;
    }
    void setNomeResponsavel(String nomeResponsavel) {
        this.nomeResponsavel = nomeResponsavel;
    }
    String getContatoResponsavel() {
        return contatoResponsavel;
    }
    void setContatoResponsavel(String contatoResponsavel) {
        this.contatoResponsavel = contatoResponsavel;
    }

    //método para adicionar os remédios na lista medicamentos. !!!Pesquisar maneiras eficazes para fazer isso!!!
    void getMedicamentos(medicacao){
       this.medicamentos.add(medicacao);
    }

    //listar os medicamentos. !!!Pesquisar maneiras "corretas" de fazer isso!!!
    String setMedicamentos(){
       return this.medicamentos.toString();
    }
}