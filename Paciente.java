public class Paciente extends Cadastro{
    int getDataInternação(int dataEntrada){
        return dataEntrada;
        // get/set e métodos afins
   }
}
class Medico extends Paciente {
   String nomeMedico;
   int CRM;
   String especialidade;
   private String tratamento;
    
   public Medico(String nomeMedico, int CRM, String especialidade){
       setNomeMedico(nomeMedico);
       setCRM(CRM);
       setEspecialidade(especialidade);
    }
   private void exibir (){
      System.out.println("----------------------------------------------------------------------");
      System.out.println("Dr(a): " + this.nomeMedico);
      System.out.println("CRM: " + this.CRM);
      System.out.println("Especialidade: " + this.especialidade);
      System.out.println("----------------------------------------------------------------------");
      System.out.println("Paciente: " + this.nomeCompleto);
      System.out.println("Idade: " + this.idade);
      System.out.println("CPF: " + this.cpf);
      System.out.println("Contato de Emergência: " + this.contatoResponsavel);
      System.out.println("Data de Internação: " + this.dataEntrada);
      System.out.println("Tratamento: " + this.tratamento);
      System.out.println("----------------------------------------------------------------------");
   } 
   void setNomeMedico(String nomeMedico) {
      this.nomeMedico = nomeMedico;     
   }
   void setCRM( int CRM){
      this.CRM = CRM;
   }
   void setEspecialidade(String especialidade) {
      this.especialidade = especialidade;    
   }
   private void setTratamento(String tratamento) {
      this.tratamento = tratamento;    
   }
}