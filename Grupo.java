public class Grupo
{
    //Adicionar parametros do tipo NomeSobrenome m_x
    String listaMembros(BiancaCarvalho m1, EmanuellePassos m2, DeboraEliane m3, JoyceCardoso m4){
      String membros = m1.nomeCompleto()+"\n"+ m2.nomeCompleto()+"\n"+ m3.nomeCompleto()+"\n"+ m4.nomeCompleto(); //adicione + m_x.nomeCompleto+"\n" (\n não é necessário para o último membro)
      return membros;
    }
    public static void main(String args[]){
      Grupo grupo = new Grupo();
      
      //instanciar NomeSobrenome
      BiancaCarvalho membroBianca = new BiancaCarvalho();
      EmanuellePassos membroEmanuelle = new EmanuellePassos();
      DeboraEliane membroDebora = new DeboraEliane();
      JoyceCardoso membroJoyce = new JoyceCardoso();
      
      //adicionar sua variavel no parametro
      System.out.println(grupo.listaMembros(membroBianca, membroEmanuelle, membroDebora, membroJoyce));
    }
}
