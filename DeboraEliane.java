 class DeboraEliane
{
    int semestre = 3;
    String nome = "Debora";
    String sobrenome = "Eliane Soares";

    public String nomeCompleto(){
       String nomeCompleto = nome + " " + sobrenome;
       return nomeCompleto;
    }
    
    public int getSemestre(){
        return semestre;
    }
}
